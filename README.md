# data-structures-visualisation-lib

Data Structure Visualization Library is a fork from https://www.cs.usfca.edu/~galles/visualization/source.html 

A part of this library is used to to animate certain data structures in VLABS experiments. Our experiments use only a part of the library which is necessary for animation and then creates an interface with the library's API to link with it.

The library could not be used in it's native form as such, so we have made some modification to the way it is structured and also changed the way the UI component are rendered and displayed. These changes have been discussed below:

1. For the purposes of a fork, the full library has been provided. However, it is possible to also use only a part of the library, as we have done for our purposes, this has been explained here [INSERT LINK].

2. The UI component of the library has been modified to make its look and feel better. Let us understand how and where are these components located.

3. There are two types of UI components - Algorithm/DS specific and Animation specific. The animation component are common across all the algorithms, however algorithm specific control change with the algorithm.

4. To change the Algorithm specific UI component go to Algorithm.js in the AlgorithmLibrary directory and locate the function addControlToAlgorithmBar, in this function we have  provided  a template code where we first identify the type of UI component we wish to change and then apply specific styles to it. The code for changing the UI component has also been embeded in the template and user can fill in the appropriate styles to customize the component accordingly.

5. To change the Animation specific controls on the UI such as skip back, play, pause, animation slider e.t.c. You can go to AnimationMain.js in AnimationLibrary directory and search for the function addControlToAnimationBar(). The code provided in this fork gives you a template to do that. User can fill in appropriate values for changing different style attribute of the element identified in the template.

# Creating a custom interface to use the library (changes to its structure)

1. Our experiments use only a part of the library which supports animating various artefacts. These pre-identified aretefacts are common to various data structures which the library is capable of realizing.

2. We do not use the library in its entirety and the exact mechanism to do so has been documented here [INSERT LINK]