
////////////////////////////////////////////////////////////
// Script to start up your function, called from the webapge:
////////////////////////////////////////////////////////////
var currentAlg;

function init()
{
	var animManag = initCanvas();
	currentAlg = new MyStackInterface(animManag, canvas.width, canvas.height);
	
}

/*We are doing a sort of "faked" inheritance within javascript. We define our function, 
set the prototype of our function to the prototype of our superclass, 
reset the constructor to be our own constructor, 
and then cache the superclass prototype, for simulating a java-style "super" call. 
Notice that to make inheritance work well on counstructors, we don't do anything 
in the main constructor function other than call an 
init function (that way we can have our init function call the init 
function of the superclass. Yes, this is a bit of a hack.)*/
function MyStackInterface(am, w, h)
{
	this.init(am, w, h);
}
MyStackInterface.prototype = new Algorithm();
MyStackInterface.prototype.constructor = MyStackInterface;
MyStackInterface.superclass = Algorithm.prototype;


//Declare some constants. We placed them in the function's namespace to avoid symbol clashes:
MyStackInterface.ELEMENT_WIDTH = 50;
MyStackInterface.ELEMENT_HEIGHT = 50;
MyStackInterface.INSERT_X = 30;
MyStackInterface.INSERT_Y = 30;
MyStackInterface.STARTING_X = 30;
MyStackInterface.STARTING_Y = 100;
MyStackInterface.FOREGROUND_COLOR = "#000055"
MyStackInterface.BACKGROUND_COLOR = "#97CB3B"

/*all of the work of the constructor is done in the init function -- 
that way constructors of subclass can effectively call the constructors of 
their superclasses. For the init function in this case, we need to do very 
little work. In this simple example we are not adding any elements to the canvas at load time. 
All we need to do is set up our own internal data structures. We are keeping track of two arrays 
-- an array that stores the actual stack (stackValues), and an array that stores the objectIDs of t
he elements of the stack (stackID)*/
MyStackInterface.prototype.init = function(am, w, h)
{
	// Call the unit function of our "superclass", which adds a couple of
	// listeners, and sets up the undo stack
	MyStackInterface.superclass.init.call(this, am, w, h);
	
	this.addControls();
	
	// Useful for memory management
	this.nextIndex = 0;

	this.stackID = [];
	this.stackValues = [];
	
	this.stackTop = 0;
}
/**
Next up is the method to add algorithm controls and callbacks. 
The tricky bit here is that we can't do something like:
this.popButton.onclick = this.popCallback
to add our callbacks. 
Why not? 
This would pass in the proper function, but not the proper contex -- essentially, 
it would be passing a pointer to the code of the function, without saving the "this" pointer. 
So we need to bind the "this" pointer to our function before we store it.
*/
MyStackInterface.prototype.addControls =  function()
{
	this.controls = [];

	
    this.pushField = addControlToAlgorithmBar("Text", "");
    this.pushField.onkeydown = this.returnSubmit(this.pushField,  
                                               this.pushCallback.bind(this), // callback to make when return is pressed
                                               4,                           // integer, max number of characters allowed in field
                                               false);                      // boolean, true of only digits can be entered.
	this.controls.push(this.pushField);
	
	this.pushButton = addControlToAlgorithmBar("Button", "Push");
	this.pushButton.onclick = this.pushCallback.bind(this);
	this.controls.push(this.pushButton);
	
	this.popButton = addControlToAlgorithmBar("Button", "Pop");
	this.popButton.onclick = this.popCallback.bind(this);
	this.controls.push(this.popButton);	
}

/*
Note that we don't do any action directly on a callback -- instead, we use the implementAction method, 
which takes a bound function (using the bind method) and a parameter, and them calls that function 
using that parameter. implementAction also saves a list of all actions that have been performed, 
so that undo will work nicely.
*/
MyStackInterface.prototype.pushCallback = function()
{
	var pushedValue = this.pushField.value;
	
	if (pushedValue != "")
	{
		this.pushField.value = "";
		this.implementAction(this.push.bind(this), pushedValue);
	}
	
}

MyStackInterface.prototype.popCallback = function()
{
	this.implementAction(this.pop.bind(this), "");
}

/**
Code that does the work. Note that we are mostly just 
implementing the action, using some calls to cmd to 
document what we are doing.
**/
MyStackInterface.prototype.push = function(pushedValue)
{
	this.commands = [];
	
	this.stackID[this.stackTop] = this.nextIndex++;
	
	this.cmd("CreateRectangle", this.stackID[this.stackTop], 
			                    pushedValue, 
								MyStackInterface.ELEMENT_WIDTH,
								MyStackInterface.ELEMENT_HEIGHT,
								MyStackInterface.INSERT_X, 
			                    MyStackInterface.INSERT_Y);
	this.cmd("SetForegroundColor", this.stackID[this.stackTop], MyStackInterface.FOREGROUND_COLOR);
	this.cmd("SetBackgroundColor", this.stackID[this.stackTop], MyStackInterface.BACKGROUND_COLOR);
	this.cmd("Step");
	var nextXPos = MyStackInterface.STARTING_X + this.stackTop * MyStackInterface.ELEMENT_WIDTH;
	var nextYPos = MyStackInterface.STARTING_Y;
	this.cmd("Move", this.stackID[this.stackTop], nextXPos, nextYPos);
	this.cmd("Step"); // Not necessary, but not harmful either
	this.stackTop++;
	return this.commands;
}

MyStackInterface.prototype.pop = function(unused)
{
	this.commands = [];
	
	if (this.stackTop > 0)
	{
		this.stackTop--;
		
		this.cmd("Move", this.stackID[this.stackTop], MyStackInterface.INSERT_X, MyStackInterface.INSERT_Y);
		this.cmd("Step");
		this.cmd("Delete", this.stackID[this.stackTop]);
		this.cmd("Step");
	}
	return this.commands;
}


/**
All visualizations must implement the reset method. 
The reset method needs to restore all of our variables to the state that they were 
in right after the call to init. In our case, we only have 2 variables of importance. 
We could have recreated the arrays stackID and stackValues, 
but that is not necessary in this case, since we don't care about the current values 
in those arrays -- we will write over any value before we read it, once the stack top is 0.
**/
MyStackInterface.prototype.reset = function()
{
	// Reset the (very simple) memory manager.
	//  NOTE:  If we had added a number of objects to the scene *before* any user
	//         input, then we would want to set this to the appropriate value based
	//         on objects added to the scene before the first user input
	this.nextIndex = 0;
	
	// Reset our data structure.  (Simple in this case)
	this.stackTop = 0;
}

// Called by our superclass when we get an animation started event -- need to wait for the
// event to finish before we start doing anything
MyStackInterface.prototype.disableUI = function(event)
{
	for (var i = 0; i < this.controls.length; i++)
	{
		this.controls[i].disabled = true;
	}
}

// Called by our superclass when we get an animation completed event -- we can
/// now interact again.
MyStackInterface.prototype.enableUI = function(event)
{
	for (var i = 0; i < this.controls.length; i++)
	{
		this.controls[i].disabled = false;
	}
}
